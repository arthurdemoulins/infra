from cleo import Application, InputOption

from command.go_shell import GoShellCommand
from command.info import InfoCommand
from command.kill import KillCommand
from command.logs import LogsCommand
from command.reset import ResetCommand
from command.restart import RestartCommand
from command.run import RunCommand
from command.set_app import SetAppCommand
from command.set_env import SetEnvCommand
from command.setup import SetupCommand
from command.shell import ShellCommand
from command.start import StartCommand
from command.stop import StopCommand
from command.up import UpCommand
from command.update import UpdateCommand
from command.web_shell import WebShellCommand
from command.container_shell import ContainerShellCommand

application = Application()

input_definition = application.get_definition()
input_definition.add_option(InputOption('--env', '-e', InputOption.VALUE_REQUIRED, 'Force environment',))
input_definition.add_option(InputOption('--app', '-a', InputOption.VALUE_REQUIRED, 'Force application',))

application.add(InfoCommand())
application.add(SetupCommand())
application.add(StartCommand())
application.add(RestartCommand())
application.add(ResetCommand())
application.add(KillCommand())
application.add(StopCommand())
application.add(ShellCommand())
application.add(SetAppCommand())
application.add(SetEnvCommand())
application.add(GoShellCommand())
application.add(UpdateCommand())
application.add(WebShellCommand())
application.add(ContainerShellCommand())
application.add(UpCommand())
application.add(RunCommand())
application.add(LogsCommand())

if __name__ == '__main__':
    application.run()
