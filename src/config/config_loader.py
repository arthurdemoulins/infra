import os
import yaml


class ConfigLoader(object):
    def __init__(self, base_dir, logger=None):
        self.base_dir = base_dir
        self.logger = logger

    def load(self, file_path):
        config = self.do_load(os.path.join(self.base_dir, file_path))
        return config

    def config_exists(self, file_path):
        return os.path.exists(os.path.join(self.base_dir, file_path))

    def write(self, file_path, data):
        filename = os.path.join(self.base_dir, file_path)
        with open(filename, 'w') as f:
            if self.logger:
                self.logger.debug("Writing to file \"{}\"".format(filename))
            f.write(yaml.dump(data))

    @staticmethod
    def do_load(file_path):
        with open(file_path, 'r') as stream:
            try:
                return yaml.load(stream)
            except yaml.YAMLError as exc:
                print("Invalid YAML in {}".format(file))
                raise exc
