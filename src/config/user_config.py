import os

from app_manager import AppManager
from config.config_loader import ConfigLoader
from option_resolver import OptionResolver


class UserConfig(object):
    config = None

    def __init__(self, root_dir, logger):
        self.config_loader = ConfigLoader(root_dir, logger)
        self.root_dir = root_dir
        self.conf_file = '.conf.yml'

    def load(self):
        try:
            config = self.config_loader.load(self.conf_file)
        except IOError:
            config = {}
        options = self.create_option_resolver()
        self.config = options.resolve(config)
        if not self.config_loader.config_exists('.conf.yml'):
            self.config_loader.write('.conf.yml', self.config)
        if os.getenv('INFRA_ENV') is not None:
            self.config['env'] = os.getenv('INFRA_ENV')

    def get_env(self):
        return self.config['env']

    def write_key(self, key, value):
        self.config[key] = value
        self.config_loader.write('.conf.yml', self.config)

    def set_key(self, key, value):
        self.config[key] = value

    def create_option_resolver(self):
        options = OptionResolver()

        def resolve_current_app(options):
            app_manager = AppManager(self.root_dir)
            apps = app_manager.get_app_list()
            if not apps:
                raise Exception('There are no application is the "{}" directory'.format(app_manager.get_apps_dir()))
            app = apps[0]
            return app

        options.set_defaults({
            "current_app": resolve_current_app,
            "env": 'dev'
        })
        options.set_allowed_types({
            "current_app": str,
            "env": str,
        })

        return options
