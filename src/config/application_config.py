import os
import re

from config.config_loader import ConfigLoader
from config.image_config import ImageConfig
from option_resolver import OptionResolver


class ApplicationConfig(object):
    def __init__(self, app_dir, user_config):
        self.app_dir = app_dir
        self.user_config = user_config
        self.options = OptionResolver()

        self.options.set_defaults({
            "docker_compose_file": "docker-compose.yml",
            "env_docker_compose_file": "docker-compose.%env%.yml",
            "volumes": {},
            "volume_permissions": [],
        })
        self.options.set_required([
            'running_services'
        ])
        self.options.set_allowed_types({
            "name": str,
            "docker_compose_file": str,
            "images": dict,
            "data_containers": dict,
            "git_repo_url": str,
            "running_services": list,
            "workspace_dir": str,
            "volume_permissions": list,
        })

    def resolve(self, app_config):
        app_config = self.options.resolve(app_config)

        app_config = self.resolve_variables(app_config, {
            "app_name": app_config['name'],
            'env': self.user_config.get_env(),
        })

        image_config = ImageConfig()
        if app_config['images'] is not None:
            for k, script in app_config['images'].iteritems():
                app_config['images'][k] = image_config.resolve(app_config['images'][k])

        return app_config

    def resolve_variables(self, config, definitions):
        seq_iter = config.iteritems() if isinstance(config, dict) else enumerate(config)

        def replace(match):
            if match.group(2) not in definitions or definitions[match.group(2)] is None:
                return ''
            return match.group(1) + definitions[match.group(2)]

        for k, v in seq_iter:
            if type(v) is str:
                config[k] = re.sub(r"([^%]|^)%([a-zA-Z][a-zA-Z_\-.0-9]*)%", replace, v)
            elif type(v) in [dict, list]:
                config[k] = self.resolve_variables(v, definitions)

        return config
