import os
import re

from config.application_config import ApplicationConfig
from config.config_loader import ConfigLoader
from option_resolver import OptionResolver
from dotenv import load_dotenv

class RootConfig(object):
    def __init__(self, app_dir, user_config):
        self.app_dir = app_dir
        self.user_config = user_config
        self.options = OptionResolver()
        self.options.set_required([
            "app",
        ])
        self.options.set_allowed_types({
            "app": dict,
        })

    def load(self, file_path):
        env_file = os.path.join(self.app_dir, self.user_config.get_env() + '.env')
        if os.path.isfile(env_file):
        	load_dotenv(env_file)

        load_dotenv(os.path.join(self.app_dir, '.env'))

        config = ConfigLoader(self.app_dir)
        config = config.load(file_path)

        config = self.options.resolve(config)

        config = self.resolve_env(config)

        app_config = ApplicationConfig(self.app_dir, self.user_config)
        config = app_config.resolve(config['app'])

        return config

    def resolve_env(self, config):
        seq_iter = config.iteritems() if isinstance(config, dict) else enumerate(config)

        def replace(match):
            return os.getenv(match.group(2))

        for k, v in seq_iter:
            if type(v) is str:
                config[k] = re.sub(r"([^\$]|^)\$([a-zA-Z][A-Z_0-9]*)", replace, v)
            elif type(v) in [dict, list]:
                config[k] = self.resolve_env(v)

        return config
