from option_resolver import OptionResolver


class ImageConfig(object):
    def __init__(self):
        self.options = OptionResolver()

        self.options.set_defaults({
        })

        self.options.set_allowed_types({
            "path": str,
            "tag": str,
        })

    def resolve(self, config):
        config = self.options.resolve(config)

        return config
