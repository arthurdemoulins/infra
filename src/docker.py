from exception.script_exception import ScriptException
from script_runner import ScriptRunner


class Docker(object):
    def __init__(self, container):
        self.logger = container.logger
        self.script_runner = ScriptRunner(self.logger)

    def build(self, path, tag):
        cmd = [
            "docker",
            "build",
            "-t", tag,
            path,
        ]

        self.script_runner.run(" ".join(cmd), shell=True)

    def run(self, args):
        cmd = [
            "docker",
            "run",
            "--rm",
        ]
        cmd.extend(args)
        self.script_runner.run(" ".join(cmd), shell=True)

    def ps(self, args):
        cmd = [
            "docker",
            "ps",
        ]
        cmd.extend(args)
        return self.script_runner.run(" ".join(cmd), shell=True)

    def kill(self, args):
        cmd = [
            "docker",
            "kill",
        ]
        cmd.extend(args)
        return self.script_runner.run(" ".join(cmd), shell=True)

    def rm(self, args):
        cmd = [
            "docker",
            "rm",
        ]
        cmd.extend(args)
        return self.script_runner.run(" ".join(cmd), shell=True)

    def create_data_container(self, container_name, volumes):
        """
        :type container_name: str
        :type volumes: list
        """
        cmd = [
            "docker",
            "create",
            "--name", container_name,
        ]

        for v in volumes:
            cmd.append('-v')
            cmd.append(v)

        cmd.append('debian:jessie')

        self.script_runner.run(" ".join(cmd), shell=True)

    def container_exists(self, container_name):
        """
        :type container_name: str
        """
        try:
            command = 'docker ps -a -f "name={}" --format "{{{{.Names}}}}" | grep {}'.format(container_name, container_name)
            output = self.script_runner.run(command, shell=True)
            for line in output:
                if line == container_name:
                    return True
            return False
        except ScriptException:
            return False
