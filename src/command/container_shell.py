import os

from command.base import BaseCommand
from docker import Docker
from docker_compose import DockerCompose


class ContainerShellCommand(BaseCommand):
    """
    Shell

    app:container-shell
    {container : Container name to go in}
    {user? : The user to login}
    """

    def handle(self):
        compose = DockerCompose(self.container)
        compose.container_shell(self.argument('container'), self.argument('user'))
