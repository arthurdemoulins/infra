import os

from command.base import BaseCommand
from docker import Docker
from docker_compose import DockerCompose


class UpCommand(BaseCommand):
    """
    Run

    app:up
    {service? : The service you want up}
    """

    def handle(self):
        compose = DockerCompose(self.container)
        service = self.argument('service')
        compose.up([
            service,
        ])
