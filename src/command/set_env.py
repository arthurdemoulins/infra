import os

from app_manager import AppManager
from command.base import BaseCommand
from config.config_loader import ConfigLoader
from config.user_config import UserConfig


class SetEnvCommand(BaseCommand):
    """
    Set environment

    set-env
    {env? : Environment you want to work with}
    """
    def handle(self):
        env = self.argument('env')
        if env:
            self.container.logger.debug("Setting env to {}".format(env))
            self.container.user_config.write_key('env', env)
