import os

from command.base import BaseCommand
from docker import Docker
from docker_compose import DockerCompose


class ShellCommand(BaseCommand):
    """
    Shell

    app:shell
    {user? : The user to login}
    """

    def handle(self):
        compose = DockerCompose(self.container)
        compose.shell(self.argument('user'))
