import os

from app_manager import AppManager
from command.base import BaseCommand
from config.config_loader import ConfigLoader
from config.user_config import UserConfig
import re
from docker import Docker
from docker_compose import DockerCompose


class GoShellCommand(BaseCommand):
    """
    Go to the current env shell and kill others

    go-shell
    {env : Environment you want to work with}
    {user? : The user to login}
    """
    def handle(self):
        env = self.argument('env')
        docker = Docker(self.container)
        containers = docker.ps([
            '--format',
            '\'{{.Names}}\'',
        ])

        current_app = self.container.user_config.config['current_app']
        pattern = r'^' + current_app + '(?:(?:pre)?prod|staging|dev)_dev_1$'
        for c in containers:
            if re.match(pattern, c):
                print 'Killing and removing docker {}'.format(c)
                docker.kill([c])
                docker.rm([c])

        self.container.user_config.set_key('env', env)
        compose = DockerCompose(self.container)
        compose.shell(self.argument('user'))
