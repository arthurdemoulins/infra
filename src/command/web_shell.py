import os

from command.base import BaseCommand
from docker import Docker
from docker_compose import DockerCompose


class WebShellCommand(BaseCommand):
    """
    Shell

    app:web-shell
    """

    def handle(self):
        compose = DockerCompose(self.container)
        compose.web_shell()
