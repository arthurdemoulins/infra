import os

from command.base import BaseCommand
from docker import Docker
from docker_compose import DockerCompose


class StopCommand(BaseCommand):
    """
    Stop

    app:stop
    {services?* : The services you want to run}
    """

    def handle(self):
        services = self.argument('services')
        compose = DockerCompose(self.container)
        compose.stop(services)
