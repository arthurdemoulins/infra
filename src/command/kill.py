import os

from command.base import BaseCommand
from docker import Docker
from docker_compose import DockerCompose


class KillCommand(BaseCommand):
    """
    Kill

    app:kill
    {services?* : The service you want to run}
    """

    def handle(self):
        services = self.argument('services')
        compose = DockerCompose(self.container)
        compose.kill(services)
