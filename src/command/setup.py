import os

from command.base import BaseCommand
from docker import Docker
from docker_compose import DockerCompose


class SetupCommand(BaseCommand):
    """
    Setup

    app:setup
    """
    def handle(self):
        docker = Docker(self.container)
        config = self.container.config
        logger = self.container.logger
        logger.info("Building images")
        for name, image in config['images'].iteritems():
            docker.build(os.path.join(self.container.app_dir, image['path']), image['tag'])

        for name, data_container in config['data_containers'].iteritems():
            if not docker.container_exists(data_container['name']):
                volumes_str = []
                for k, volume in enumerate(data_container['volumes']):
                    volume_str = volume['dest']
                    if "local" in volume:
                        if not volume['local']:
                            continue
                        volume_str = "{}:{}".format(volume['local'], volume_str)
                    volumes_str.append(volume_str)

                self.line("Creating data container <info>{}</info>".format(name))
                docker.create_data_container(data_container['name'], volumes_str)

                for k, volume in enumerate(data_container['volumes']):
                    if "chown" in volume:
                        docker.run([
                            '--volumes-from {}'.format(data_container['name']),
                            'debian:jessie',
                            'chown -R {} {}'.format(volume['chown'], volume['dest']),
                        ])
            else:
                logger.info('Container {} already exists, skipping...'.format(data_container['name']))

        compose = DockerCompose(self.container)
        compose.build()

        for perm in config['volume_permissions']:
            compose.run([
                perm['image'],
                'chown -R {} {}'.format(perm['chown'], perm['path']),
            ])

