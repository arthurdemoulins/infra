import os

from app_manager import AppManager
from command.base import BaseCommand
from script_runner import ScriptRunner


class UpdateCommand(BaseCommand):
    """
    Reset

    update
    """

    default_branch = 'master'
    script_runner = None

    def handle(self):
        self.script_runner = ScriptRunner(self.container.logger)
        self.update_infra()
        self.update_apps()

    def update_infra(self):
        self.update_git_repo('Infra', self.container.get_root_dir(), self.default_branch)

    def update_apps(self):
        app_manager = AppManager(self.container.get_root_dir())
        apps = app_manager.get_app_list()

        for app in apps:
            self.update_git_repo(app, os.path.join(self.container.get_root_dir(), 'apps', app), self.default_branch)

    def update_git_repo(self, app_name, path, default_branch):
        current_branch = self.script_runner.run(' '.join([
            'git',
            '-C', '{}'.format(path),
            'rev-parse --abbrev-ref HEAD'
        ]), shell=True)[0]

        if current_branch != default_branch:
            self.line(
                '<fg=yellow>{} is currently on the <fg=yellow;options=bold>{}</> branch.</>'.format(
                    app_name,
                    current_branch
                )
            )
            self.line('You must be on the <comment>{}</comment> one to update!'.format(self.default_branch))
            exit(1)

        output = self.script_runner.run(' '.join([
            'git',
            '-C', '{}'.format(path),
            'pull'
        ]), shell=True)

        if output[0] == 'Already up-to-date.':
            self.line('<options=bold>{}</> is up-to-date!'.format(app_name))
        else:
            self.line('<fg=green;options=bold>{}</><fg=green> has been updated!</>'.format(app_name))
