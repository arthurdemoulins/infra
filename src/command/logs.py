import os

from command.base import BaseCommand
from docker import Docker
from docker_compose import DockerCompose


class LogsCommand(BaseCommand):
    """
    Kill

    app:logs
    {services* : The service you want to run}
    """

    def handle(self):
        services = self.argument('services')
        compose = DockerCompose(self.container)
        compose.logs(['-f', ' '.join(services)])
