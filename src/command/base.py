from cleo import Command, Output
import logging
import os

from config.root_config import RootConfig
from config.user_config import UserConfig
from container import Container


class BaseCommand(Command):

    container = None

    def __init__(self):
        super(BaseCommand, self.__class__).__init__(self)

    def create_container(self):
        self.container = Container()
        self.container.logger = self.create_logger()
        self.container.user_config = UserConfig(self.get_root_dir(), self.container.logger)
        self.container.user_config.load()
        if self.option('app'):
            self.container.user_config.set_key('current_app', self.option('app'))
        if self.option('env'):
            self.container.user_config.set_key('env', self.option('env'))

        self.container.config = self.create_config()
        self.container.app_dir = self.get_current_app_dir()

    def create_logger(self):
        logger = logging.getLogger()

        verbosity = self.output.get_verbosity()
        if verbosity == Output.VERBOSITY_VERBOSE:
            logger.setLevel(logging.WARN)
        elif verbosity == Output.VERBOSITY_VERY_VERBOSE:
            logger.setLevel(logging.INFO)
        elif verbosity == Output.VERBOSITY_DEBUG:
            logger.setLevel(logging.DEBUG)

        logging.addLevelName(logging.WARNING, "\033[1;33m%s\033[1;0m" % logging.getLevelName(logging.WARNING))
        logging.addLevelName(logging.INFO, "\033[1;34m%s\033[1;0m" % logging.getLevelName(logging.INFO))
        logging.addLevelName(logging.ERROR, "\033[1;31m%s\033[1;0m" % logging.getLevelName(logging.ERROR))

        ch = logging.StreamHandler()
        formatter = logging.Formatter('%(asctime)s [%(levelname)s]: %(message)s')
        ch.setFormatter(formatter)
        logger.addHandler(ch)

        return logger

    def create_config(self):
        config = RootConfig(self.get_current_app_dir(), self.container.user_config)
        config = config.load('config/config.yml')

        return config

    def get_current_app_dir(self):
        if not self.container.user_config.config['current_app']:
            return None

        return os.path.join(self.get_apps_dir(), self.container.user_config.config['current_app'])

    def initialize(self, input_, output_):
        self.create_container()

    def get_root_dir(self):
        return os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', '..')

    def get_apps_dir(self):
        return os.path.join(self.get_root_dir(), 'apps')
