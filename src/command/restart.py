from command.base import BaseCommand
from docker_compose import DockerCompose


class RestartCommand(BaseCommand):
    """
    Restart

    app:restart
    {services?* : The services you want to restart}
    """

    def handle(self):
        services = self.argument('services')
        compose = DockerCompose(self.container)
        compose.restart(services)
