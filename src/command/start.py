import os

from command.base import BaseCommand
from docker import Docker
from docker_compose import DockerCompose


class StartCommand(BaseCommand):
    """
    Start

    app:start
    {services?* : The services you want to run}
    """

    def handle(self):
        services = self.argument('services')
        compose = DockerCompose(self.container)
        compose.start(services)
