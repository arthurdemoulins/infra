import os

from app_manager import AppManager
from command.base import BaseCommand
from config.config_loader import ConfigLoader
from config.user_config import UserConfig


class SetAppCommand(BaseCommand):
    """
    Set application

    set-app
    {app? : Current application name you want to work with}
    """
    def handle(self):
        app_list = self.get_app_list()
        app = self.argument('app')
        if app:
            if app not in app_list:
                self.print_list()
                raise Exception('App "{}" does not exist'.format(app))
            self.container.logger.debug("Setting current_app to {}".format(app))
            self.container.user_config.write_key('current_app', app)

    def get_app_list(self):
        app_manager = AppManager(self.container.get_root_dir())
        return app_manager.get_app_list()

    def print_list(self):
        self.line('Available applications:')
        for p in enumerate(self.get_app_list()):
            self.line('  - {}'.format(p))
