import os

from command.base import BaseCommand
from docker import Docker
from docker_compose import DockerCompose


class ResetCommand(BaseCommand):
    """
    Reset

    app:reset
    {services?* : The services you want reset}
    """
    def handle(self):
        compose = DockerCompose(self.container)
        services = self.argument('services')
        compose.reset(services)
