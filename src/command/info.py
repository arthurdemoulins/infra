from command.base import BaseCommand

class InfoCommand(BaseCommand):
    """
    Get info

    app:info
    {user? : The user to login}
    """
    def handle(self):
        print 'Current app: ' + self.container.user_config.config['current_app']
        print 'Current env: ' + self.container.user_config.config['env']
