import os

from command.base import BaseCommand
from docker import Docker
from docker_compose import DockerCompose


class RunCommand(BaseCommand):
    """
    Run

    app:run
    {service : The service you want to run}
    {cmd? : The command you want to run}
    """

    def handle(self):
        compose = DockerCompose(self.container)
        service = self.argument('service')
        cmd = self.argument('cmd')
        args = [
            service,
        ]
        if cmd:
            args.append(cmd)
        compose.run(args)
