import os


class AppManager(object):
    def __init__(self, root_dir):
        self.root_dir = root_dir

    def get_app_list(self):
        apps_dir = self.get_apps_dir()
        apps = os.listdir(apps_dir)
        for k, p in enumerate(apps):
            if not os.path.isdir(os.path.join(apps_dir, p)):
                del apps[k]

        return apps

    def get_apps_dir(self):
        return os.path.join(self.root_dir, 'apps')
