import subprocess
import time
from logging import Logger

from exception.script_exception import ScriptException


class ScriptRunner(object):
    def __init__(self, logger):
        """
        :type logger: Logger
        """
        self.logger = logger

    def run(self, cmd, shell=None, filter_output=None):
        if type(cmd) is str:
            cmd_str = cmd
        else:
            cmd_str = ' '.join(cmd)

        self.logger.info("Run command > {}".format(cmd_str))

        buff = []
        try:
            process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=shell)
            for line in iter(process.stdout.readline, ''):
                if filter_output is not None:
                    line = filter_output(line)
                if line is not None:
                    self.logger.debug(line.rstrip())
                    buff.append(line.rstrip())
                    if len(buff) > 15:
                        buff.pop(0)
            while process.poll() is None:
                time.sleep(0.1)

            if process.returncode != 0:
                raise ScriptException("Script returned a non-zero code", "\n".join(buff))

            return buff

        except subprocess.CalledProcessError as error:
            raise ScriptException("Error while running command: {}".format(error), "\n".join(buff))

    def call(self, cmd, shell=None):
        if type(cmd) is str:
            cmd_str = cmd
        else:
            cmd_str = ' '.join(cmd)
        self.logger.info("Call command > {}".format(cmd_str))

        if shell:
            cmd = cmd_str

        subprocess.call(cmd, shell=shell)
