class ScriptException(Exception):
    def __init__(self, message, last_output):
        self.message = message
        self.last_output = last_output

    def __str__(self):
        return "{}:\n{}".format(self.message, self.last_output)
