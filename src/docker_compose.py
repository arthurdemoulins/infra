import os

from script_runner import ScriptRunner


class DockerCompose(object):
    def __init__(self, container):
        self.logger = container.logger
        self.app_dir = container.app_dir
        config = container.config

        self.user_config = container.user_config
        self.compose_file = config['docker_compose_file']
        self.env_compose_file = config['env_docker_compose_file']
        self.app_name = config['name']
        self.running_services = config['running_services']
        self.script_runner = ScriptRunner(self.logger)

        self.container_app_dir = os.path.join(config['workspace_dir'], config['name'])

    def start(self, services=None):
        args = ['up', '-d']

        if services:
            args.extend(services)
        else:
            args.extend(self.running_services)

        cmd = self.get_command_base(args)
        self.script_runner.call(' '.join(cmd), shell=True)

    def ps(self, args):
        cmd = self.get_command_base(['ps'])
        cmd.extend(args)
        return self.script_runner.call(' '.join(cmd), shell=True)

    def logs(self, args):
        cmd = self.get_command_base(['logs'])
        cmd.extend(args)
        return self.script_runner.call(' '.join(cmd), shell=True)

    def up(self, args):
        cmd = self.get_command_base([
            'up', '-d'
        ])
        cmd.extend(args)
        self.script_runner.call(' '.join(cmd), shell=True)

    def run(self, args):
        cmd = self.get_command_base([
            'run', '--rm'
        ])
        cmd.extend(args)
        self.script_runner.call(' '.join(cmd), shell=True)

    def run_dev(self, args, cwd=None):
        if cwd is None:
            cwd = self.container_app_dir

        cmd = self.get_command_base([
            'run',
            '--rm',
            '--workdir={}'.format(cwd),
            'dev',
        ])
        cmd.extend(args)
        self.script_runner.call(' '.join(cmd), shell=True)

    def stop(self, services):
        cmd = self.get_command_base([
            'stop'
        ])
        cmd.extend(services)
        self.script_runner.call(' '.join(cmd), shell=True)

    def kill(self, services):
        cmd = self.get_command_base([
            'kill'
        ])
        cmd.extend(services)
        self.script_runner.call(' '.join(cmd), shell=True)

    def restart(self, services):
        args = ['restart']

        if services:
            args.extend(services)
        else:
            args.extend(self.running_services)

        cmd = self.get_command_base(args)
        self.script_runner.call(' '.join(cmd), shell=True)

    def rm(self, args):
        cmd = self.get_command_base([
            'rm'
        ])
        cmd.extend(args)
        self.script_runner.call(' '.join(cmd), shell=True)

    def reset(self, args):
        self.kill(args)
        self.rm(args)
        #self.start()

    def shell(self, user=None):
        self.start()

        cmd = self.get_command_base([
            'up', '-d', 'dev'
        ])

        self.script_runner.call(' '.join(cmd), shell=True)

        cmd = self.get_command_base([
            'exec',
        ])

        if user is not None:
            cmd.extend(['--user', user])

        cmd.extend([
            'dev', '/bin/zsh'
        ])

        self.script_runner.call(' '.join(cmd), shell=True)
        exit(0)

    def web_shell(self):
        self.start()

        cmd = self.get_command_base([
            'exec', 'web', '/bin/bash'
        ])
        self.script_runner.call(' '.join(cmd), shell=True)
        exit(0)


    def container_shell(self, container_name, user=None):
        self.start()

        cmd = self.get_command_base([
            'exec',
        ])

        if user is not None:
            cmd.extend(['--user', user])

        cmd.extend([
            container_name, '/bin/bash'
        ])

        self.script_runner.call(' '.join(cmd), shell=True)
        exit(0)

    def build(self):
        cmd = self.get_command_base([
            'build'
        ])
        self.script_runner.run(' '.join(cmd), shell=True)

    def get_command_base(self, extend=None):
        cmd = [
        	'APP_ENV='+self.user_config.get_env(),
            'docker-compose',
            '--file', os.path.join(self.app_dir, self.compose_file),
            '--file', os.path.join(self.app_dir, self.env_compose_file),
            '--project-name', '{}{}'.format(self.app_name, self.user_config.get_env())
        ]

        if extend is not None:
            cmd.extend(extend)

        return cmd
