import os
from logging import Logger

from config.root_config import RootConfig
from config.user_config import UserConfig


class Container(object):
    """
    :type logger: Logger
    :type config: RootConfig
    :type user_config: UserConfig
    """

    logger = None
    config = None
    user_config = None

    def get_root_dir(self):
        return os.path.join(os.path.dirname(os.path.realpath(__file__)), "..")
