class OptionResolver(object):
    def __init__(self):
        self.allowed = {}
        self.defaults = {}
        self.allowed_types = {}
        self.required = []

    def resolve(self, config):
        for k in self.defaults:
            if k not in config:
                config[k] = self.defaults[k]

        for k in config:
            if callable(config[k]):
                try:
                    config[k] = config[k](config)
                except TypeError:
                    continue

        for k in config:
            if callable(config[k]):
                config[k] = config[k](config)

        for k in self.required:
            if k not in config or config[k] is None:
                raise Exception("Missing or empty node '{}'".format(k))

        for k in self.allowed_types:
            if k in config:
                if type(config[k]) != self.allowed_types[k] and config[k] is not None:
                    raise Exception(
                        "Node '{}' is not a '{}'. Got '{}'".format(k, self.allowed_types[k], type(config[k]))
                    )
            else:
                config[k] = None

        for k in config:
            if not (k in self.allowed and self.allowed[k] == 1):
                raise Exception("Unknown key '{}'".format(k))

        return config

    def set_defaults(self, defaults):
        self.defaults = defaults
        self.add_allowed(defaults.keys())

    def set_allowed_types(self, allowed_types):
        self.allowed_types = allowed_types
        self.add_allowed(allowed_types)

    def set_required(self, required):
        self.required = required
        for a in required:
            self.allowed[a] = 1

    def add_allowed(self, allowed):
        for a in allowed:
            self.allowed[a] = 1
