#!/bin/bash
set -e

mkdir -p ~/ssl/
openssl genrsa -des3 -out ~/ssl/rootCA.key 2048
openssl req -x509 -new -nodes -key ~/ssl/rootCA.key -sha256 -days 1825 \
    -subj "/C=FR/ST=France/O=Arthem, Inc./CN=Arthem" \
    -out ~/ssl/rootCA.pem

echo "Done."
