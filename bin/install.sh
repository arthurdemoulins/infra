#!/bin/bash

PROJECT_DIR="$( cd "$(dirname "$0")/.." && pwd )"
pushd $PROJECT_DIR > /dev/null

pip install --user -r requirements.txt

popd > /dev/null
