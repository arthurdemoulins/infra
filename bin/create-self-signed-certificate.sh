#!/bin/bash
set -e

PROJECT_DIR="$( cd "$(dirname "$0")/.." && pwd )"
echo $PROJECT_DIR
SSL_DIR="/etc/nginx/ssl/sf.local"

sudo mkdir -p $SSL_DIR

sudo openssl req -new -sha256 -nodes -out $SSL_DIR/sf.local.csr -newkey rsa:2048 -keyout $SSL_DIR/sf.local.key \
    -config $PROJECT_DIR/nginx/ssl/server.csr.cnf

sudo openssl x509 -req -in $SSL_DIR/sf.local.csr -CA ~/ssl/rootCA.pem -CAkey ~/ssl/rootCA.key -CAcreateserial \
    -out $SSL_DIR/sf.local.crt -days 1825 -sha256 -extfile $PROJECT_DIR/nginx/ssl/v3.ext

sudo rm $SSL_DIR/sf.local.csr

sudo service nginx configtest
sudo service nginx reload

echo "Done."
