# Infra

## Prerequisites

Python 2.7 and PIP installed.

To install PIP run:

```bash
sudo apt-get install python-pip
```

## Installation

Install PIP requirements:

```bash
pip install --user -r requirements.txt
```

Add your applications:

```bash
git clone git@gitserver.com:user/infra-myapp.git apps/myapp/
```

Initialize:

```bash
./bin/console app:setup
```

### SSL for development

https://serversforhackers.com/video/self-signed-ssl-certificates-for-development


```bash
sudo mkdir /etc/ssl/sf.local
cd /etc/ssl/sf.local
sudo openssl genrsa -out "/etc/ssl/sf.local/sf.local.key" 2048
sudo openssl req -new -key "/etc/ssl/sf.local/sf.local.key" -out "/etc/ssl/sf.local/sf.local.csr"
> Country Name: FR
> State: France
> City: Paris
> Organization Name: sf.local
> Organizational Unit: Arthem
> Common Name: *.sf.local
> Email: contact@arthem.eu
> Password:
> Company Name: Arthem
sudo openssl x509 -req -days 365 \
    -in "/etc/ssl/sf.local/sf.local.csr" \
    -signkey "/etc/ssl/sf.local/sf.local.key" \
    -out "/etc/ssl/sf.local/sf.local.crt"
```
